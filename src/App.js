import React, { Component } from 'react';

class CheckBox extends Component {
  state = {
    clicked: false
}

showIdentification = () => 
this.state.clicked ? 
  this.setState({ clicked: false }) 
  :
  this.setState({ clicked: true })
  
render() {
  return (
    <div className='checkbox'> Check:
        {this.state.clicked === true ?
        <button onClick={this.showIdentification} className='button-next'></button> 
        : 
        <button onClick={this.showIdentification} className='button'></button> }
      </div>
    )
  }
}

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      items: [],
      isLoaded: false, 
    }
  }

componentDidMount() {
  fetch('https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json')
  .then(result => result.json())
  .then(json => {
    this.setState({
      isLoaded: true,
      items: json, 
    })
  });
}

render() {
  let { isLoaded, items } = this.state;
  if(!isLoaded) {
    return <div className='loader loader-animated'></div>;
  } 
  else {
    return (
      <div className="main">
        <div className='contacts'>Contacts</div>
        <div className="search__form"><input className='form' type='text' placeholder='Search...'></input></div>
        <ul className="user__list">
          {items.map(item => (
            <li className="user" key={item.id}>
              <img className='user__avatar' src={item.avatar} alt='avatar-pic'></img>
              <p className='user__name'>{item.first_name}</p>
              <p className='user__last-name'>{item.last_name}<CheckBox /></p>
            </li>
          ))};
        </ul>
      </div>
      )
    }
  }
}

export default App;
